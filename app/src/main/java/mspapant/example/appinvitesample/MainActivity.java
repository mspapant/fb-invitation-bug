package mspapant.example.appinvitesample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;

public class MainActivity extends AppCompatActivity {

    public CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FacebookSdk.sdkInitialize(getApplicationContext());

        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                callbackManager = CallbackManager.Factory.create();
                FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
                String appLinkUrl = "https://fb.me/522116304640135";
                String previewImageUrl = "http://tagmosaic.com/img/inviteF.png";
                if (AppInviteDialog.canShow()) {
                    AppInviteContent content = new AppInviteContent.Builder()
                            .setApplinkUrl(appLinkUrl)
                            .setPreviewImageUrl(previewImageUrl)
                            .build();

                    AppInviteDialog appInviteDialog = new AppInviteDialog(MainActivity.this);
                    appInviteDialog.registerCallback(callbackManager,
                            new FacebookCallback<AppInviteDialog.Result>() {
                                @Override
                                public void onSuccess(AppInviteDialog.Result result) {
                                    Toast.makeText(MainActivity.this, "Invitation Sent Successfully!", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onCancel() {
                                    Toast.makeText(MainActivity.this, "Invitation Cancel!", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onError(FacebookException e) {
                                    Log.d("Result", "Error " + e.getMessage());
                                    Toast.makeText(MainActivity.this, "Invitation Error!", Toast.LENGTH_LONG).show();
                                }
                            });

                    FacebookSdk.setIsDebugEnabled(true);
                    appInviteDialog.show(content);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
